# LXQT Themes
A collection of themes that I made for the lxqt desktop
## win-eleven-dark :
- A theme inspired by the default windows 11 dark theme without transparency / blur

### Screenshot :

![Screenshot](win-eleven-dark.png)

### Installation :
- You can download the theme folder from this page and extract it into /usr/share/lxqt/themes or ~/.local/share/lxqt/themes
- In order to have the transparency, rounded corner etc... you must use breeze, kvantum or oxygene as qt theme as explained in the [lxqt wiki](https://github.com/lxqt/lxqt/wiki/Theming#lxqt-themes).
- You should also disable shadow for menus in kvantum (if you use kvantum) to avoid shadow being displayed around widgets.
- By default the icons in the panel will be very litle, to get a better look you can increase  the dashboard height and icon size (right click on the dashboard -> configure -> size and icon size) depending of the resolution of your monitor. For example in this screenshot the dashboard height is 60px and the icons are 32px on a 1080p screen

## Copyrights :
- Based on sombre-et-rond lxqt theme and dark lxqt theme https://github.com/lxqt/lxqt-themes/tree/master/themes/dark
- The LXQt logo was designed by @Caig and is licensed CC-BY-SA 3.0.
- All Icons under win-eleven-dark/icons/*.svg except mainmenu :
MIT License

Copyright (c) 2020 Microsoft Corporation

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

## sombre-et-rond :
- A dark, rounded and modern lxqt theme based on dark lxqt theme https://github.com/lxqt/lxqt-themes/tree/master/themes/dark and yaru rounded theme for lxqt 
- Button from Lubuntu Arc LXQT theme
- Buttons from https://material.io/resources/icons 
### Screenshot :

![Screenshot](sombre-et-rond.png)

### Installation :
You can download the theme folder from this page and extract it into /usr/share/lxqt/themes or ~/.local/share/lxqt/themes
